using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
    public float speed = 40f;

    public float speedIncreaseByPoint;
       // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<DetectCollision>() != null)
        {
            Destroy(gameObject);
            return;
        }

        if (other.gameObject.name != "Food")
        {
            return;
        }
        GameManager.inst.IncrementScore();
    }

}
