using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    int score;
    public static GameManager inst;

    public Text scoreText;
    public MoveForward moveForward;

    public void IncrementScore()
    {
        score++;
        scoreText.text = "SCORE : " + score;
        // increase player's speed
        moveForward.speed += moveForward.speedIncreaseByPoint;
    }
    // Start is called before the first frame update
    void Awake()
    {
        inst = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
